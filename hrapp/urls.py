from django.urls import path
from hrapp import views

urlpatterns = [
    path('/', views.homepage, name="homepage"),
    path('contact/', views.contact, name="contact"),
    # employee
    path('employee/', views.employee_list, name="employee_list"),
    path('employee/create', views.create_employee, name="create_employee"),
    path('employee/<int:id>/status/', views.employee_status, name="employee_status"),
    path('employee/<int:id>/view/', views.employee_view, name="employee_view"),
    path('employee/<int:id>/edit/', views.employee_edit, name="employee_edit"),
    path('employee/payment/add/<int:id>/', views.employee_payment_add, name="employee_payment_add"),
    path('employee/payment/edit/<int:id>/', views.employee_payment_edit, name="employee_payment_edit"),

    #Category
    path('category/', views.category_list, name="category_list"),
    path('category/create/', views.create_category, name="create_category"),
    path('category/edit/<int:id>/', views.edit_category, name="edit_category"),
    path('category/delete/<int:id>/<str:slug>/', views.delete_category, name="delete_category"),
    path('category/status/<int:id>/', views.category_status, name="category_status"),

    path('root_category/', views.root_category_list, name="root_category_list"),
    path('root_category/create/', views.root_category_create, name="root_category_create"),
    path('root_category/edit/<int:id>/', views.edit_root_category, name="edit_root_category"),
    path('root_category/delete/<int:id>/', views.delete_root_category, name="delete_root_category"),
    path('root_category/status/<int:id>/', views.root_category_status, name="root_category_status"),

    #Order
    path('orders/', views.order_list, name="order_list"),
    path('order/create/', views.create_order, name="create_order"),
    # path('/category/delete/<int:id>/<str:slug>/', views.delete_category, name="delete_category"),

    #Rider Payment
    path('payments/', views.employee_payment_list, name="employee_payment_list"),
    path('payments/edit/<int:id>/', views.payments_edit, name="payments_edit"),
    path('payments/status/<int:id>/', views.payments_status, name="payments_status"),

    # Email
    path('send_email/', views.send_email, name="send_email"),
    #path('invoice/', views.send_email_invoice, name="send_email_invoice"),
]