from statistics import mode
from django.db import models

# Create your models here.

class Employee(models.Model):
    user_id             = models.IntegerField(default=0)
    first_name          = models.CharField(max_length=100, null=True)
    last_name           = models.CharField(max_length=100, null=True)
    email               = models.CharField(max_length=100)
    phone               = models.CharField(max_length=100)
    date_of_birth       = models.DateField(blank=True, null=True)
    age                 = models.IntegerField(default=0)
    blood_group         = models.CharField(max_length=20, null=True)
    gender              = models.IntegerField(default=0)
    nationality         = models.CharField(max_length=50, null=True)
    religion            = models.CharField(max_length=30, null=True)
    passport            = models.CharField(max_length=50, null=True)
    driving_license     = models.CharField(max_length=50, null=True)
    fathers_name        = models.CharField(max_length=50, null=True)
    mothers_name        = models.CharField(max_length=50, null=True)
    national_id         = models.IntegerField(null=True)
    marital_Status      = models.IntegerField(default=0)
    spouse_name         = models.CharField(max_length=50, null=True)
    no_of_childen       = models.IntegerField(default=0)
    is_staff            = models.IntegerField(default=0)
    present_address     = models.CharField(max_length=200, null=True)
    comment             = models.CharField(max_length=300, null=True)
    permanent_address   = models.CharField(max_length=250, null=True)
    profile_photo       = models.FileField(upload_to="profile_image/", blank=True, null=True)  
    is_active           = models.BooleanField(default=True)
    date_of_joing       = models.DateField(blank=True, null=True)
    status              = models.BooleanField(default=1)
#Rider Payment
class RiderPayment(models.Model):
    employee            = models.ForeignKey(Employee, on_delete=models.DO_NOTHING)
    month               = models.DateField(null=True,blank=True)
    order_qty           = models.DecimalField(max_digits=5, decimal_places=2,default=0)
    pay_per_order       = models.DecimalField(max_digits=5, decimal_places=2,default=0)
    tips                = models.DecimalField(max_digits=5, decimal_places=2,default=0)
    late_log_in         = models.DecimalField(max_digits=5, decimal_places=2,default=0)
    no_show_card        = models.DecimalField(max_digits=5, decimal_places=2,default=0)
    office_deduction    = models.DecimalField(max_digits=5, decimal_places=2,default=0)
    intallment_due      = models.DecimalField(max_digits=5, decimal_places=2,default=0)
    toal_amount         = models.DecimalField(max_digits=5, decimal_places=2,default=0)
    status              = models.BooleanField(default=1)
    rider_payment_image = models.ImageField(upload_to="rider_payment_image/", blank=True, null=True) 
    created_at          = models.DateTimeField(auto_now_add=True)
    updated_at          = models.DateTimeField(auto_now=False,null=True,blank=True)
    updated_by          = models.IntegerField(default=0,null=True,blank=True)

    # def get_employee_name(self):
    #     emp_info = Employee.objects.get(id=self.employee_id)
    #     return str(emp_info.first_name) + " " + str(emp_info.last_name)
    

#Root Category Model
class RootCategory(models.Model):
    name                =models.CharField(max_length=100)
    is_active           = models.BooleanField(default=1)
    created_at          = models.DateTimeField(auto_now_add=True)
    updated_at          = models.DateTimeField(auto_now=False,null=True,blank=True)
    updated_by          = models.IntegerField(default=0,null=True,blank=True)
  

#Cateogry Model
class Cateogry(models.Model):
    root_category    = models.ForeignKey(RootCategory, on_delete=models.DO_NOTHING)
    name                = models.CharField(max_length=100)
    slug                = models.CharField(max_length=100)
    description         = models.CharField(max_length=300, null=True)
    category_photo       = models.FileField(upload_to="category_image/", blank=True, null=True) 
    is_active           = models.BooleanField(default=1)
    created_at          = models.DateTimeField(auto_now_add=True)
    updated_at          = models.DateTimeField(auto_now=False,null=True,blank=True)
    updated_by          = models.IntegerField(default=0,null=True,blank=True)
    status              = models.IntegerField(default=0,null=True,blank=True)

