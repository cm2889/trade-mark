


import os

from django.shortcuts import render


from django.shortcuts import redirect, render

from trademark.settings import EMAIL_HOST_USER
from . import models

from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.contrib.auth import authenticate, get_user_model


# Email
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives
from django.http import HttpResponse

User = get_user_model()
# Create your views here.

def homepage(request):
    return render(request, 'home/index.html')


def contact(request):
    return render(request, 'home/contact.html')

#Employee 
def create_employee(request):
    if request.method == "GET":
        
        return render(request, 'employee/index.html')
    elif request.method == "POST": 
        email   = request.POST.get('email')
        first_name = request.POST.get('first_name')
        last_name   = request.POST.get('last_name') 

        user = User.objects.create(username=email, email=email, first_name=first_name, last_name=last_name, is_active=True)
        user.set_password(email)
        user.save()

        
        phone   = request.POST.get('phone')
        date_of_birth   = request.POST.get('date_of_birth')
        age   = request.POST.get('age')
        blood_group   = request.POST.get('blood_group')
        gender   = request.POST.get('gender')
        nationality   = request.POST.get('nationality')
        religion   = request.POST.get('religion')
        passport   = request.POST.get('passport')
        driving_license   = request.POST.get('driving_license')
        fathers_name   = request.POST.get('fathers_name')
        mothers_name   = request.POST.get('mothers_name')
        national_id   = request.POST.get('national_id')
        marital_Status   = request.POST.get('marital_Status')
        spouse_name   = request.POST.get('spouse_name')
        no_of_childen   = request.POST.get('no_of_childen')
        is_staff   = request.POST.get('is_staff')
        present_address   = request.POST.get('present_address')
        permanent_address   = request.POST.get('permanent_address')
        comment   = request.POST.get('comment')
        print("first_name: ", first_name, ", phone: ", phone,",comment: ",comment)


        #Profile photo added starts here
        profile_photo  = ""
        if bool(request.FILES.get('profile_photo', False)) == True:
            file = request.FILES['profile_photo']  
            if not os.path.exists("hrapp/static/media/profile_photo/"):
                os.mkdir("hrapp/static/media/profile_photo/")

            profile_photo = default_storage.save("profile_photo/"+file.name, ContentFile(file.read()))
            if profile_photo:
                    profile_photo = str("profile_photo/")+str(profile_photo).split("/")[-1]
        #Profile photo ends here

        models.Employee.objects.create(user_id=user.id, first_name=first_name,last_name=last_name,email=email,phone=phone,
        date_of_birth=date_of_birth,age=age,blood_group=blood_group,gender=gender,nationality=nationality,religion=religion,
        passport=passport,driving_license=driving_license,mothers_name=mothers_name,fathers_name=fathers_name,
        national_id=national_id,marital_Status=marital_Status,spouse_name=spouse_name,no_of_childen=no_of_childen,
        is_staff=is_staff,present_address=present_address,permanent_address=permanent_address,comment=comment, profile_photo=profile_photo)

        return render(request, 'employee/index.html')
def employee_list(request):
    if request.method == "GET":
        employee = models.Employee.objects.all()
        context = {
            "employee": employee 
        }
        return render(request, 'employee/employee-list.html',context)
    elif request.method == "POST": 
        return render(request, 'employee/index.html')

def employee_view(request, id):
    if request.method == "GET":
        data = models.Employee.objects.get( id = id )
        context = {
            "data": data 
        }
        return render(request, 'employee/show.html', context)

    elif request.method == "POST": 
        return render(request, 'employee/show.html')


def employee_payment_add(request,id):
    if request.method == "GET":
        data = models.Employee.objects.get( id = id )
       
        context = {
            "data": data,
            "emp_id": id
        }

        return render(request, 'employee/payment/payment-add.html',context)

    elif request.method == "POST":
       
        get_date = request.POST.get('month')+"-01"
        employee_id = request.POST.get('employee_id')
        order_qty = request.POST.get('order_qty')
        pay_per_order = request.POST.get('pay_per_order')
        tips = request.POST.get('tips')
        late_log_in = request.POST.get('late_log_in')
        no_show_card = request.POST.get('no_show_card')
        office_deduction = request.POST.get('office_deduction')
        intallment_due = request.POST.get('intallment_due')
        status = request.POST.get('status')
        toal_amount = request.POST.get('toal_amount')

        payment_image  = ""
        if bool(request.FILES.get('rider_payment_image', False)) == True:
            file = request.FILES['rider_payment_image']  
            if not os.path.exists("hrapp/static/media/payment_image/"):
                os.mkdir("hrapp/static/media/payment_image/")

            payment_image = default_storage.save("payment_image/"+file.name, ContentFile(file.read()))
            if payment_image:
                    payment_image = str("payment_image/")+str(payment_image).split("/")[-1]

        payment =models.RiderPayment.objects.create( 
           # month= get_date,
            order_qty=order_qty, 
            pay_per_order=pay_per_order, 
            tips=tips, 
            late_log_in=late_log_in,
            no_show_card=no_show_card,
            office_deduction=office_deduction,
            intallment_due=intallment_due,
            status=status,
            toal_amount=toal_amount,
            employee_id=employee_id,
            rider_payment_image=payment_image,
        ) 
        print(payment)

        data = models.Employee.objects.get(id = id)
       
        context = {
            "data": data,
            "emp_id": id,
            "payment": payment

        }
        subject = "Monthly Invoice"
       
        html_body = render_to_string("mail/email.html",context)

        msg = EmailMultiAlternatives(subject=subject, from_email=EMAIL_HOST_USER, to=[str(data.email), "mo7rul@gmail.com", "monirul.islam@isho.com"])
    
        msg.attach_alternative(html_body, "text/html")
        msg.send()
        return render(request, 'employee/payment/payment-add.html', context)

# payments view
def payments_edit(request,id):
    if request.method == "GET":
        payments_view = models.RiderPayment.objects.get(id=id)
        employee_data = models.Employee.objects.get(id=id)

        print('Image path :',payments_view.status )

        context = {
            'data': payments_view,
            'data_employee':employee_data
        }
        return render(request, 'employee/payment/payment_edit.html',context)

    elif request.method == "POST":
        get_date = request.POST.get('month')+"-01"
        # get_date = request.POST.get('month')
        employee_id = request.POST.get('employee_id')
        order_qty = request.POST.get('order_qty')
        pay_per_order = request.POST.get('pay_per_order')
        tips = request.POST.get('tips')
        late_log_in = request.POST.get('late_log_in')
        no_show_card = request.POST.get('no_show_card')
        office_deduction = request.POST.get('office_deduction')
        intallment_due = request.POST.get('intallment_due')
        status = request.POST.get('status')
        toal_amount = request.POST.get('toal_amount')

        rider_payment_image  = ""
        if bool(request.FILES.get('rider_payment_image', False)) == True:
            file = request.FILES['rider_payment_image']  
            if not os.path.exists("hrapp/static/media/payment_image/"):
                os.mkdir("hrapp/static/media/payment_image/")
            rider_payment_image = default_storage.save("payment_image/"+file.name, ContentFile(file.read()))
            if rider_payment_image:
                    rider_payment_image = str("payment_image/")+str(rider_payment_image).split("/")[-1]

        models.RiderPayment.objects.filter(id=id).update( 
            month= get_date,
            order_qty=order_qty, 
            pay_per_order=pay_per_order, 
            tips=tips, 
            late_log_in=late_log_in,
            no_show_card=no_show_card,
            office_deduction=office_deduction,
            intallment_due=intallment_due,
            status=status,
            toal_amount=toal_amount,
            employee_id=employee_id,
            rider_payment_image=rider_payment_image,
        )  
        return redirect('/hr/payments/')

#Payment list
def employee_payment_list(request):
    if request.method == "GET":
        #data = models.Employee.objects.get( id = id )
        rider_data = models.RiderPayment.objects.all()
       
        context = {
            "rider_data": rider_data
        }

        return render(request, 'employee/payment/payment_list.html',context)
#Payment Status
def payments_status(request, id):
    data = models.RiderPayment.objects.get(id = id)

    if data.status == 1:

        models.RiderPayment.objects.filter(id=id).update(

            status = 0,
             )
        return redirect('/hr/payments/')
    else:

          models.RiderPayment.objects.filter(id=id).update(
            status = 1,

          )
          return redirect('/hr/payments')
    

def employee_payment_edit(request,id):
    if request.method == "GET":
        data = models.RiderPayment.objects.get( id = id )
        # datetime.datetime.strptime(data.month, "%Y-%m-%d")
        context = {
            "data": data,
        }

        return render(request, 'employee/payment-edit.html',context)
    

def employee_edit(request, id):
    if request.method == "GET":
        data = models.Employee.objects.get( id = id )
        context = {
            "data": data,
        }
        return render(request, 'employee/employee-edit.html', context)

    elif request.method == "POST": 
        email           = request.POST.get('email')
        first_name      = request.POST.get('first_name')
        last_name       = request.POST.get('last_name') 
        phone           = request.POST.get('phone')
        date_of_birth   = request.POST.get('date_of_birth')
        age             = request.POST.get('age')
        blood_group     = request.POST.get('blood_group')
        gender          = request.POST.get('gender')
        nationality     = request.POST.get('nationality')
        religion        = request.POST.get('religion')
        passport        = request.POST.get('passport')
        driving_license = request.POST.get('driving_license')
        fathers_name    = request.POST.get('fathers_name')
        mothers_name    = request.POST.get('mothers_name')
        national_id     = request.POST.get('national_id')
        marital_Status  = request.POST.get('marital_Status')
        spouse_name     = request.POST.get('spouse_name')
        no_of_childen   = request.POST.get('no_of_childen')
        is_staff        = request.POST.get('is_staff')
        present_address = request.POST.get('present_address')
        permanent_address   = request.POST.get('permanent_address')
        comment         = request.POST.get('comment')
        profile_photo  = ""
        if bool(request.FILES.get('profile_photo', False)) == True:
            file = request.FILES['profile_photo']  
            if not os.path.exists("hrapp/static/media/profile_photo/"):
                os.mkdir("hrapp/static/media/profile_photo/")
            profile_photo = default_storage.save("profile_photo/"+file.name, ContentFile(file.read()))
            if profile_photo:
                    profile_photo = str("profile_photo/")+str(profile_photo).split("/")[-1]

        models.Employee.objects.filter(id=id).update( 
            first_name=first_name,last_name=last_name, email=email, phone=phone,date_of_birth=date_of_birth, age=age, blood_group=blood_group,gender=gender,nationality=nationality,religion=religion,
        passport=passport,driving_license=driving_license,mothers_name=mothers_name,fathers_name=fathers_name,
        national_id=national_id,marital_Status=marital_Status,spouse_name=spouse_name,no_of_childen=no_of_childen,
        is_staff=is_staff,present_address=present_address,permanent_address=permanent_address,comment=comment, profile_photo=profile_photo
        )

        return redirect('/hr/employee/')

def employee_status(request, id):
    data = models.Employee.objects.get(id = id)

    if data.status == 1:

        models.Employee.objects.filter(id=id).update(

            status = 0,
             )
        return redirect('/employee/')
    else:

          models.Employee.objects.filter(id=id).update(
            status = 1,

          )
          return redirect('/employee')

def category_list(request):
    if request.method == "GET":
        categories = models.Cateogry.objects.all()
        context = {
            "categories": categories,
        }
        return render(request, 'category/index.html', context)        
    
def root_category_list(request):
    if request.method == "GET":
        categories = models.RootCategory.objects.all()
        context = {
            "categories": categories,
        }
        return render(request, 'category/root/index.html', context)       
#create category 
def create_category(request):
    
    if request.method == "GET":
        root_category = models.RootCategory.objects.filter(is_active=1)
        context = {
            "root_category": root_category,
        }
        return render(request, 'category/create.html',context)        

    elif request.method == "POST": 


        root_category_id=request.POST.get('root_category_id')
        name            = request.POST.get('name')
        slug            = request.POST.get('slug')
        description     = request.POST.get('description') 
        is_active       = request.POST.get('is_active') 
        status          = request.POST.get('status') 
         

        category_photo  = ""
        if bool(request.FILES.get('category_photo', False)) == True:
            file = request.FILES['category_photo']  
            if not os.path.exists("hrapp/static/media/category_photo/"):
                os.mkdir("hrapp/static/media/category_photo/")

            category_photo = default_storage.save("category_photo/"+file.name, ContentFile(file.read()))
            if category_photo:
                    category_photo = str("category_photo/")+str(category_photo).split("/")[-1]

        models.Cateogry.objects.create(name=name, slug=slug, description=description, is_active=is_active,  root_category_id=root_category_id,category_photo=category_photo,status=status)
        
        return redirect('/hr/category/')
#Root Category Create
def root_category_create(request):
    
    if request.method == "GET":
        return render(request, 'category/root/create.html')        

    elif request.method == "POST": 
        name            = request.POST.get('name')
        is_active       = request.POST.get('is_active') 
        models.RootCategory.objects.create(name=name, is_active=is_active)
        
        return redirect('/hr/root_category/')
#Edit Root Category
def edit_root_category(request, id):
    
    data = models.RootCategory.objects.get(id = id)

    if request.method == "GET":

        contact = {
            'data' : data
        }
        return render(request, 'category/root/edit.html', contact)        

    elif request.method == "POST": 
        
        name            = request.POST.get('name')
        is_active       = request.POST.get('is_active') 
        models.RootCategory.objects.filter(id = id).update(name=name,  is_active=is_active)
        return redirect('/hr/root_category/')
#Root category status
def root_category_status(request, id):
    data = models.RootCategory.objects.get(id = id)

    if data.is_active == 1:

        models.RootCategory.objects.filter(id=id).update(

            is_active = 0,
             )
        return redirect('/hr/root_category/')
    else:

          models.RootCategory.objects.filter(id=id).update(
            is_active = 1,

          )
          return redirect('/hr/root_category')
#Category Status
def category_status(request, id):
    data = models.Cateogry.objects.get(id = id)

    if data.is_active == 1:

        models.Cateogry.objects.filter(id=id).update(

            is_active = 0,
             )
        return redirect('/hr/category/')
    else:

          models.Cateogry.objects.filter(id=id).update(
            is_active = 1,

          )
          return redirect('/hr/category')      
#Edit Category
def edit_category(request, id):

    data = models.Cateogry.objects.get(id = id)
    root_categories = models.RootCategory.objects.all()

    if request.method == "GET":

        context = {
            'data' : data,
            'root_categories' : root_categories,
        }
        return render(request, 'category/edit.html', context)        

    elif request.method == "POST": 
        
        name            = request.POST.get('name')
        slug            = request.POST.get('slug')
        description     = request.POST.get('description') 
        is_active       = request.POST.get('is_active') 
        root_category_id=request.POST.get('root_category_id')
        status          =request.POST.get('status')
        

        category_photo  = ""
        if bool(request.FILES.get('category_photo', False)) == True: 

            file = request.FILES['category_photo'] 
            if not os.path.exists("hrapp/static/media/category_photo/"):
                os.mkdir("hrapp/static/media/category_photo/")
            category_photo = default_storage.save(file.name, ContentFile(file.read()))
            if category_photo:
                    category_photo = str(category_photo).split("/")[-1]

        if category_photo == '':
            models.Cateogry.objects.filter(id = id).update(name=name, slug=slug, description=description, is_active=is_active,root_category_id=root_category_id,status=status)
            return redirect('/hr/category/')

        elif category_photo != '':
            models.Cateogry.objects.filter(id = id).update(name=name, slug=slug, description=description, is_active=is_active, root_category_id=root_category_id, category_photo=category_photo,status=status)
            return redirect('/hr/category/')
#Root Category delete
def delete_root_category(request, id):
    
    deleted = models.RootCategory.objects.filter(id=id)
    if deleted:
        deleted.delete()
        return redirect('/hr/root_category/')
    else:
        return redirect('/hr/category/create/')
#Category Delete
def delete_category(request, slug, id):
    
    deleted = models.Cateogry.objects.filter(id=id, slug = slug)
    if deleted:
        deleted.delete()
        return redirect('/hr/category/')
    else:
        return redirect('/hr/category/create/')

def order_list(request):
    if request.method == "GET":
        orders = models.RiderPayment.objects.all()
        context = {
            "orders": orders,
        }
        return render(request, 'order/index.html', context)        
    elif request.method == "POST": 
        
        return render(request, 'employee/index.html')

def create_order(request):
    if request.method == "GET":
        return render(request, 'order/create.html')        


# def send_email(request):
#     to = 'mo7rul@gmail.com'
#     print(to)
#     message_body = """Dear Sir/Madam, Please revise your submitted order then you are going payment.,"""
#     send_mail('Notification | Notification email', message_body, EMAIL_HOST_USER, [to, 'monirul.islam@isho.com'])
#     return HttpResponse('Notification email send successful')

def send_email(request):
    # email = request.POST.get('email')
    # id = request.POST.get('employee_id')
    # subject = request.POST.get('subject')
    # message = request.POST.get('message')
    context = {
        'user':'Monirul',
    }

    subject = "Test Report"
    text_body = "Test Report"
    html_body = render_to_string("mail/email.html", context)

    msg = EmailMultiAlternatives(subject=subject, from_email=EMAIL_HOST_USER, to=["monirul89@gmail.com", "mo7rul@gmail.com", "monirul.islam@isho.com"], body=text_body)
    
    msg.attach_alternative(html_body, "text/html")
    msg.send()

    return HttpResponse('Successfull Mail Send')
